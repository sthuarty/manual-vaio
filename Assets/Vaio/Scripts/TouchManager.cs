using UnityEngine;
using UnityEngine.InputSystem;

public class TouchManager : Singleton<TouchManager> {
    [SerializeField] private bool debug = false;

    public PlayerInputActions InputActions => _inputActions;
    private PlayerInputActions _inputActions;

    public Vector2 TouchScreenPosition => _touchScreenPosition = IsTouching ? _inputActions.Player.TouchPosition.ReadValue<Vector2>() : Vector2.zero;
    [SerializeField] private Vector2 _touchScreenPosition;

    public Vector3 TouchWorldPosition => _touchWorldPosition;
    [SerializeField] private Vector3 _touchWorldPosition;

    public bool IsTouching => _isTouching;
    [SerializeField] private bool _isTouching = false;

    public GameObject HittedObject => _hittedObject;
    [SerializeField] private GameObject _hittedObject;

    private GameObject touchPositionDebugObject;

    [SerializeField] private Canvas canvas;


    public delegate void Event();
    public event Event TouchStart;
    public event Event TouchEnd;

    public override void Awake() {
        base.Awake();
        _inputActions = new PlayerInputActions();
    }

    private void OnEnable() {
        _inputActions.Enable();
        _inputActions.Player.TouchPress.started += ctx => StartTouch(ctx);
        _inputActions.Player.TouchPress.canceled += ctx => EndTouch(ctx);
    }

    private void OnDisable() {
        _inputActions.Disable();
        _inputActions.Player.TouchPress.started -= ctx => StartTouch(ctx);
        _inputActions.Player.TouchPress.canceled -= ctx => EndTouch(ctx);
    }

    private void Update() {
        if (debug) {
            if (IsTouching) {
                if (IsRayCollindingWithSomeObject()) {
                    touchPositionDebugObject.transform.position = _touchWorldPosition;
                    Debug.DrawRay(RayTouch().origin, RayTouch().direction * 10.0f, Color.red);
                }
            }
        }
    }

    private Ray RayTouch() => Camera.main.ScreenPointToRay(TouchScreenPosition);

    private void StartTouch(InputAction.CallbackContext ctx) {
        if (debug) {
            GameObject temp = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            Destroy(temp.GetComponent<Collider>());
            temp.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            temp.GetComponent<MeshRenderer>().material.color = Color.red;
            touchPositionDebugObject = temp;
        }

        _isTouching = true;
        TouchStart?.Invoke();
    }

    private void EndTouch(InputAction.CallbackContext ctx) {
        if (debug) Destroy(touchPositionDebugObject);

        _hittedObject = null;
        _touchWorldPosition = Vector3.zero;
        _touchScreenPosition = Vector3.zero;

        _isTouching = false;
        TouchEnd?.Invoke();
    }

    private bool IsRayCollindingWithSomeObject() {
        RaycastHit hit;
        if (Physics.Raycast(RayTouch(), out hit)) {
            _touchWorldPosition = RayTouch().GetPoint(hit.distance);
            _hittedObject = hit.transform.gameObject;
            return true;
        }
        return false;
    }

    public bool IsToutching(RectTransform _object) {
        Vector3[] corners = new Vector3[4];
        _object.GetWorldCorners(corners);

        for (int i = 0; i < corners.Length; i++)
            corners[i] = Camera.main.WorldToScreenPoint(corners[i]);

        float xMin = corners[0].x;
        float xMax = corners[3].x;
        float yMin = corners[0].y;
        float yMax = corners[1].y;

        if (TouchScreenPosition.x > xMin && TouchScreenPosition.x < xMax &&
            TouchScreenPosition.y > yMin && TouchScreenPosition.y < yMax)
            return true;
        else return false;
    }
}
