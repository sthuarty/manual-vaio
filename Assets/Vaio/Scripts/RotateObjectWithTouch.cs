using System;
using System.Collections;
using UnityEngine;

public class RotateObjectWithTouch : MonoBehaviour {
    [SerializeField] private Camera _camera;
    [SerializeField] private bool rotateHorizontally = true;
    [SerializeField] private bool rotateVertically = true;
    [SerializeField] private float moveSmoothness = 0.3f;
    [SerializeField] private float touchSensitivity = 0.5f;
    [SerializeField] private Vector2 prevPos, posDelta = Vector2.zero;
    [SerializeField] private RectTransform rotateArea;

    private Spinning spinning;
    private Quaternion startRotation;
    private bool hasRotated;


    private void Awake() {
        if (_camera == null) _camera = Camera.main;

        startRotation = transform.localRotation;

        spinning = GetComponent<Spinning>();
    }

    private void Start() => TouchManager.Instance.TouchEnd += TouchEnd;
    private void OnApplicationQuit() => TouchManager.Instance.TouchEnd -= TouchEnd;

    private void TouchEnd() {
        if (spinning != null) spinning.enabled = true;
        if (hasRotated) { BackToOriginRotation(); hasRotated = false; }
        prevPos = Vector2.zero;
    }

    private void Update() {
        if (TouchManager.Instance.IsToutching(rotateArea)) {

            if (prevPos.magnitude > 0) {
                posDelta = (TouchManager.Instance.TouchScreenPosition - prevPos);

                StopAllCoroutines();

                if (posDelta.magnitude > touchSensitivity) {
                    if (rotateVertically)
                        transform.Rotate(_camera.transform.right, Vector3.Dot(posDelta, _camera.transform.up), Space.World);

                    if (rotateHorizontally) {
                        if (Vector3.Dot(transform.up, Vector3.up) >= 0)
                            transform.Rotate(transform.up, -Vector3.Dot(posDelta, _camera.transform.right), Space.World);
                        else
                            transform.Rotate(transform.up, Vector3.Dot(posDelta, _camera.transform.right), Space.World);
                    }

                    if (spinning != null) spinning.enabled = false;
                    hasRotated = true;
                }
            }

            prevPos = TouchManager.Instance.TouchScreenPosition;
        }
    }

    public void BackToOriginRotation() => StartCoroutine(BackToOriginRotationCoroutine());
    private IEnumerator BackToOriginRotationCoroutine() {
        float t = 0f;
        Quaternion startRot = transform.localRotation;
        while (t <= 1f) {
            t += Time.deltaTime / moveSmoothness;
            Quaternion newRotation = Quaternion.Euler(new Vector3(startRotation.eulerAngles.x, transform.rotation.eulerAngles.y, startRotation.eulerAngles.z));
            transform.localRotation = Quaternion.Lerp(startRot, newRotation, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }
}
