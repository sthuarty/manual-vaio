namespace CommonTypes {
    public delegate void Event();
    public delegate void EventString(string value);
}