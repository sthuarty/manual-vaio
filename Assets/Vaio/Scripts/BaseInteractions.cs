using System.Collections;
using UnityEngine;

public class BaseInteractions : MonoBehaviour {
    public float moveSmoothness = 0.1f;

    [HideInInspector] public Vector3 originPosition;
    [HideInInspector] public Quaternion originRotation;


    protected virtual void Awake() {
        originPosition = transform.position;
        originRotation = transform.rotation;
    }

    public IEnumerator SmoothMove(Vector2 startPos, Vector2 endPos, float seconds) {
        float t = 0f;
        while (t <= 1.0f) {
            t += Time.deltaTime / seconds;
            transform.position = Vector2.Lerp(startPos, endPos, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }

    public IEnumerator SmoothMove(Vector3 startPos, Vector3 endPos, float seconds) {
        float t = 0f;
        while (t <= 1.0f) {
            t += Time.deltaTime / seconds;
            transform.position = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }

    public IEnumerator SmoothMove(Vector3 startPos, Quaternion startRot, Vector3 endPos, Quaternion endRot, float seconds) {
        float t = 0f;
        while (t <= 1.0f) {
            t += Time.deltaTime / seconds;
            transform.position = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0f, 1f, t));
            transform.rotation = Quaternion.Lerp(startRot, endRot, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }

    public void BackToOriginPosition() => StartCoroutine(BackToOriginPositionCoroutine());

    private IEnumerator BackToOriginPositionCoroutine() {
        Vector3 startPos = transform.position;
        Quaternion startRot = transform.rotation;
        float t = 0f;
        while (t <= 1.0f) {
            t += Time.deltaTime / moveSmoothness;
            transform.position = Vector3.Lerp(startPos, originPosition, Mathf.SmoothStep(0f, 1f, t));
            transform.rotation = Quaternion.Lerp(startRot, originRotation, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }

    public void SmoothMoveWithRotation(Transform startTransform, Transform endTransform, float seconds) => StartCoroutine(SmoothMoveWithRotationCoroutine(startTransform, endTransform, seconds));
    public IEnumerator SmoothMoveWithRotationCoroutine(Transform startTransform, Transform endTransform, float seconds) {
        Vector3 startPos = startTransform.position;
        Quaternion startRot = startTransform.rotation;
        float t = 0f;
        while (t <= 1.0f) {
            t += Time.deltaTime / seconds;
            transform.position = Vector3.Lerp(startPos, endTransform.position, Mathf.SmoothStep(0f, 1f, t));
            transform.rotation = Quaternion.Lerp(startRot, endTransform.rotation, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }

    public IEnumerator FadeTo(float destinationAlpha, float _time, bool thenDisable) {
        float originAlpha = transform.GetComponent<SpriteRenderer>().material.color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / _time) {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(originAlpha, destinationAlpha, t));
            GetComponent<SpriteRenderer>().material.color = newColor;
            yield return null;
        }
        if (thenDisable) this.gameObject.SetActive(false);
    }
}
