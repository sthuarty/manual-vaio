using CommonTypes;

static public class GameManager {

    static public event Event ProductChangeEvent;

    static private ProductScriptableObject product;
    static public ProductScriptableObject Product {
        get { return product; }
        set {
            if (value != product) {
                product = value;
                ProductChangeEvent?.Invoke();
            }
        }
    }
}
