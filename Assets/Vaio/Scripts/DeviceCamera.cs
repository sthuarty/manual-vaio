using UnityEngine;
using UnityEngine.UI;

public class DeviceCamera : MonoBehaviour {
    [SerializeField] private RawImage background;
    [SerializeField] private AspectRatioFitter fit;
    [SerializeField] private int onEditorTestCameraIndex = 1;

    [HideInInspector] public WebCamTexture cameraTexture;

    private bool camAvaliable;
    private Texture defaultBackGround;


    public void Start() {
        defaultBackGround = background.texture;
        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0) {
            Debug.Log("No camera detected");
            camAvaliable = false;
            return;
        }

        if (Application.platform == RuntimePlatform.WindowsEditor)
            cameraTexture = new WebCamTexture(devices[onEditorTestCameraIndex].name, Screen.width, Screen.height);
        else {
            foreach (WebCamDevice device in devices) {
                if (!device.isFrontFacing)
                    cameraTexture = new WebCamTexture(device.name, Screen.width, Screen.height);
            }

            if (cameraTexture == null) {
                Debug.Log("Unable to find back camera.");
                return;
            }
        }

        cameraTexture.Play();
        background.texture = cameraTexture;

        camAvaliable = true;
    }

    private void Update() {
        if (!camAvaliable)
            return;

        float ratio = (float)cameraTexture.width / (float)cameraTexture.height;
        fit.aspectRatio = ratio;

        float scaleY = cameraTexture.videoVerticallyMirrored ? -1f : 1f;
        background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

        int orient = -cameraTexture.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0f, 0f, orient);
    }

    private void OnDisable() => cameraTexture?.Pause();

    private void OnDestroy() => cameraTexture?.Stop();
}
