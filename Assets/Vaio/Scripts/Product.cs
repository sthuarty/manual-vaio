using System.Collections.Generic;
using UnityEngine;

public class Product : MonoBehaviour {
    public List<PointOfInterest> pointOfInterestList = new List<PointOfInterest>();
}

[System.Serializable]
public struct PointOfInterest {
    public string name;
    // [TextArea] public string description;
    public Transform cameraPosition;
}