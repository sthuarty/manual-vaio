using UnityEngine;

public class MenuSceneManager : MonoBehaviour {
    // Chamado no botão da UI
    public void StartVirtualProductTour() => SceneLoader.Instance.LoadSceneWithFadeEffect("Main");

    // Chamado no botão da UI
    public void SeeManual() => SceneLoader.Instance.LoadSceneWithFadeEffect("PDFViewer");

    // Chamado no botão da UI
    public void Quit() => Application.Quit();
}
