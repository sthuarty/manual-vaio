using UnityEngine;

[CreateAssetMenu(fileName = "Category-ProductName", menuName = "ScriptableObjects/Product")]
public class ProductScriptableObject : ScriptableObject {
    public string _name;
    public string qRCodeContent;
    public GameObject prefab;
    public string pDFFileName;
}
