using System.Collections.Generic;
using UnityEngine;

public class ResourcesManager : Singleton<ResourcesManager> {
    [HideInInspector] public List<ProductScriptableObject> allProductsList;


    public override void Awake() {
        base.Awake();
        allProductsList = GetAllProductScriptableObjects();
    }

    private List<ProductScriptableObject> GetAllProductScriptableObjects() {
        List<ProductScriptableObject> result = new List<ProductScriptableObject>();

        foreach (ProductScriptableObject product in Resources.LoadAll("", typeof(ProductScriptableObject)))
            result.Add(product);

        return result;
    }
}
