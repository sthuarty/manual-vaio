using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

///<sumary>
/// Lembrar de adicionar as cenas no Scenes do Build Settings
///</sumary>
public class SceneLoader : Singleton<SceneLoader> {

    public string menuSceneName = "Menu";
    public string splashScreenSceneName = "Splash Screen";
    [SerializeField] private Animator transition;
    [SerializeField] private float transitionTime = 1f;

    private string CurrentSceneName => SceneManager.GetActiveScene().name;


    public bool IsAtSplashScreenScene => CurrentSceneName == splashScreenSceneName;
    public bool IsAtMenuScene => CurrentSceneName == menuSceneName;


    public void LoadSceneWithFadeEffect(string sceneName) => StartCoroutine(LoadSceneWithFadeEffectCoroutine(sceneName));
    private IEnumerator LoadSceneWithFadeEffectCoroutine(string sceneName) {
        if (transition != null) {
            transition.SetTrigger("FadeIn");
            yield return new WaitForSeconds(transitionTime);

            SceneManager.LoadScene(sceneName);

            // yield return new WaitForSeconds(transitionTime / 2);
            transition.SetTrigger("FadeOut");

        } else {
            SceneManager.LoadScene(sceneName);
        }
    }
}
