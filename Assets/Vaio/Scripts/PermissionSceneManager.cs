using UnityEngine;
using UnityEngine.Android;

public class PermissionSceneManager : MonoBehaviour {
    private bool hasRequestedPermission;


    // É Chamado assim que o código começa a rodar.
    // Depois do Awake, do Enable e antes do Start
    private void OnApplicationFocus(bool _) {
        if (Application.platform == RuntimePlatform.Android && hasRequestedPermission) {
            if (Permission.HasUserAuthorizedPermission(Permission.Camera))
                SceneLoader.Instance.LoadSceneWithFadeEffect("QRReader");
        }
    }

    private void Start() {
        if (Application.platform != RuntimePlatform.Android)
            SceneLoader.Instance.LoadSceneWithFadeEffect("QRReader");
    }

    //  Assinado no botão da UI
    public void RequestCameraAccess() {
        if (Application.platform == RuntimePlatform.Android && !Permission.HasUserAuthorizedPermission(Permission.Camera)) {
            hasRequestedPermission = true;
            Permission.RequestUserPermission(Permission.Camera);
        } else
            SceneLoader.Instance.LoadSceneWithFadeEffect("QRReader");
    }
}
