using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MainSceneManager : Singleton<MainSceneManager> {
    [SerializeField] private BaseInteractions cameraInterations;
    [SerializeField] private GameObject productParentGameObject;
    [SerializeField] private GameObject buttonsPanel;
    [SerializeField] private GameObject buttonPrefab;

    private Spinning productSpinner;
    private RotateObjectWithTouch productTouchRotator;
    private ProductScriptableObject product;
    private GameObject productGameObject;


    public override void Awake() {
        productSpinner = productParentGameObject.GetComponent<Spinning>();
        productTouchRotator = productParentGameObject.GetComponent<RotateObjectWithTouch>();
    }

    private void Start() {
        product = GetProduct();
        InstantiateProductGameObject();
        PopulateButtonsPanel();
    }

    private ProductScriptableObject GetProduct() {
        ProductScriptableObject value = GameManager.Product != null ? GameManager.Product : ResourcesManager.Instance.allProductsList[0];

        if (value != null)
            return value;
        else {
            Debug.LogError("MainSceneManager.cs: Não foi possível carregar o produto pois não foi encontrado nenhum.");
            return null;
        }
    }

    private void InstantiateProductGameObject() {
        foreach (Transform child in productParentGameObject.transform)
            GameObject.Destroy(child.gameObject);

        productGameObject = Instantiate(product.prefab, productParentGameObject.transform);
    }

    private void PopulateButtonsPanel() {
        ClearButtonsPanel();
        GenerateButton("Visão geral", () => ViewDefault());
        foreach (PointOfInterest pointOfInterest in productGameObject.GetComponent<Product>().pointOfInterestList)
            GenerateButton(pointOfInterest.name, () => FocusOnPointOfInterest(pointOfInterest));
    }

    void GenerateButton(string text, UnityAction onClickFunction) {
        GameObject button = Instantiate(buttonPrefab, buttonsPanel.transform);
        button.GetComponentInChildren<TMP_Text>().text = text;
        button.GetComponent<Button>().onClick.AddListener(onClickFunction);
    }

    private void ClearButtonsPanel() {
        foreach (Transform child in buttonsPanel.transform)
            GameObject.Destroy(child.gameObject);
    }

    private void FocusOnPointOfInterest(PointOfInterest pointOfInterest) {
        productSpinner.StopAllCoroutines();
        cameraInterations.StopAllCoroutines();

        productTouchRotator.enabled = false;
        productSpinner.StopSpinning();
        productSpinner.BackToOriginRotation();

        cameraInterations.SmoothMoveWithRotation(cameraInterations.transform, pointOfInterest.cameraPosition, cameraInterations.moveSmoothness);
    }

    private void ViewDefault() {
        productSpinner.StopAllCoroutines();
        cameraInterations.StopAllCoroutines();

        productTouchRotator.enabled = true;
        productSpinner.StartSpinning();
        cameraInterations.BackToOriginPosition();
    }

    //  Chamado no botão da UI
    public void BackToMenu() => SceneLoader.Instance.LoadSceneWithFadeEffect("Menu");
}
