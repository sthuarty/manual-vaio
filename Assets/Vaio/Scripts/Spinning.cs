using System.Collections;
using UnityEngine;

public enum AxisEnum { xAxis, yAxis, zAxis };
public class Spinning : MonoBehaviour {
    [SerializeField] private bool isSpinning = true;
    [SerializeField] private float speed = 20f;
    [SerializeField] private AxisEnum axis = AxisEnum.yAxis;
    [SerializeField] private bool isClockwiseDirection = true;
    [SerializeField] private float moveSmoothness = 0.3f;

    private Vector3 direction;
    private Quaternion startRotation;


    private void Awake() {
        startRotation = transform.localRotation;

        switch (axis) {
            case AxisEnum.xAxis:
                direction = isClockwiseDirection ? Vector3.right : Vector3.left;
                break;
            case AxisEnum.yAxis:
                direction = isClockwiseDirection ? Vector3.up : Vector3.down;
                break;
            case AxisEnum.zAxis:
                direction = isClockwiseDirection ? Vector3.forward : Vector3.back;
                break;
        }
    }

    private void FixedUpdate() {
        if (isSpinning)
            transform.Rotate(direction, Time.deltaTime * speed);
    }

    public void StartSpinning() => isSpinning = true;
    public void StopSpinning() => isSpinning = false;

    public void BackToOriginRotation() {
        StopSpinning();
        StartCoroutine(BackToOriginRotationCoroutine());
    }

    private IEnumerator BackToOriginRotationCoroutine() {
        float t = 0f;
        Quaternion startRot = transform.localRotation;
        while (t <= 1f) {
            t += Time.deltaTime / moveSmoothness;
            transform.localRotation = Quaternion.Lerp(startRot, startRotation, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }
}
