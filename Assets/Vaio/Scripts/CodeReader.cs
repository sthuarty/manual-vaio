using System.Threading;
using CommonTypes;
using UnityEngine;
using ZXing;
using ZXing.QrCode;

public class CodeReader : Singleton<CodeReader> {
    public event EventString codeReadedEvent;

    [SerializeField] private DeviceCamera deviceCamera;

    private Texture2D encoded;
    private Thread qrThread;
    private Color32[] c;
    private int W, H;
    private bool isQuit;
    private string LastResult;
    private bool shouldEncodeNow;


    private void Start() {
        encoded = new Texture2D(256, 256);
        LastResult = "http://www.google.com";
        shouldEncodeNow = true;

        qrThread = new Thread(DecodeQR);
        qrThread.Start();
    }

    private void Update() {
        if (deviceCamera.cameraTexture != null) {
            W = deviceCamera.cameraTexture.width;
            H = deviceCamera.cameraTexture.height;
        }

        if (c == null) {
            c = deviceCamera.cameraTexture.GetPixels32();
        }

        // encode the last found
        var textForEncoding = LastResult;
        if (shouldEncodeNow &&
            textForEncoding != null) {
            var color32 = Encode(textForEncoding, encoded.width, encoded.height);
            encoded.SetPixels32(color32);
            encoded.Apply();
            shouldEncodeNow = false;
        }
    }

    public void StopReading() => isQuit = true;

    private void DecodeQR() {
        // create a reader with a custom luminance source
        var barcodeReader = new BarcodeReader { AutoRotate = false, Options = new ZXing.Common.DecodingOptions { TryHarder = false } };

        while (true) {
            if (isQuit)
                break;

            try {
                // decode the current frame
                var result = barcodeReader.Decode(c, W, H);
                if (result != null) {
                    LastResult = result.Text;
                    shouldEncodeNow = true;
                    Debug.Log($"QR Code readed: {result.Text}");
                    codeReadedEvent?.Invoke(result.Text);
                }

                // Sleep a little bit and set the signal to get the next frame
                Thread.Sleep(200);
                c = null;
            } catch {
            }
        }
    }

    private static Color32[] Encode(string textForEncoding, int width, int height) {
        var writer = new BarcodeWriter {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }

    private void OnApplicationQuit() => StopReading();

    private void OnDestroy() => qrThread.Abort();
}
