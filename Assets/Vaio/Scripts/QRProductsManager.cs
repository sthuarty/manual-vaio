using UnityEngine;

public class QRProductsManager : MonoBehaviour {
    [SerializeField] private DeviceCamera deviceCamera;
    void Awake() => UnityThread.initUnityThread();

    private void Start() => CodeReader.Instance.codeReadedEvent += CodeReaded;
    private void OnDisable() => CodeReader.Instance.codeReadedEvent -= CodeReaded;

    private void CodeReaded(string code) {
        CodeReader.Instance.StopReading();
        // A função que lê o QR roda em outra thread, o código abaixo faz a função "SetProductByCodeScanned" ser chamada na main thread.
        UnityThread.executeInUpdate(() => { SetProductByCodeScanned(code); });
    }

    private void SetProductByCodeScanned(string code) {
        ProductScriptableObject product = ResourcesManager.Instance.allProductsList.Find(x => x.qRCodeContent == code);

        if (product != null) {
            GameManager.Product = product;
            SceneLoader.Instance.LoadSceneWithFadeEffect("Menu");
        } else
            Debug.LogWarning("O código escaneado não está cadastrado na lista de códigos de produtos.");
    }
}
