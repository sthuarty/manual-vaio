using Paroxe.PdfRenderer;
using UnityEngine;

public class PDFSceneManager : MonoBehaviour {
    public PDFViewer pDFViewer;


    private void Start() {
        pDFViewer.FileSource = PDFViewer.FileSourceType.StreamingAssets;
        pDFViewer.Folder = "PDFs";
        pDFViewer.FileName = GetPDFFileName();
        pDFViewer.LoadDocument();
    }

    private string GetPDFFileName() {
        string pDFFileName = GameManager.Product?.pDFFileName;

        if (string.IsNullOrEmpty(pDFFileName))
            pDFFileName = ResourcesManager.Instance.allProductsList[0].pDFFileName;

        if (string.IsNullOrEmpty(pDFFileName)) {
            Debug.LogError("PDFSceneManager.cs: Não foi possível carregar o PDF pois não foi encontrado nenhum produto.");
            return null;
        }

        if (pDFFileName.Length >= 5) {
            string extension = pDFFileName.Substring(pDFFileName.Length - 4);
            if (!string.Equals(extension, ".pdf"))
                pDFFileName = string.Concat(pDFFileName, ".pdf");
        } else
            pDFFileName = string.Concat(pDFFileName, ".pdf");

        return pDFFileName;
    }

    //  Chamados nos botões da UI
    public void BackToMenu() => SceneLoader.Instance.LoadSceneWithFadeEffect("Menu");
    public void ZoomIn() => pDFViewer.ZoomIn();
    public void ZoomOut() => pDFViewer.ZoomOut();
}
