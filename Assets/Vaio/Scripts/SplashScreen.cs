using System.Collections;
using UnityEngine;
using UnityEngine.Android;

public class SplashScreen : MonoBehaviour {
    [SerializeField] private float splashScreenDelayTime = 3f;
    [SerializeField] private string nextSceneName = "Main";

    private bool hasRequestedPermission;

    private void Awake() => Screen.sleepTimeout = SleepTimeout.NeverSleep;


    // É Chamado assim que o código começa a rodar.
    // Depois do Awake, do Enable e antes do Start
    private void OnApplicationFocus(bool _) {
        if (Application.platform == RuntimePlatform.Android && hasRequestedPermission) {

            if (Permission.HasUserAuthorizedPermission(Permission.Camera))
                SceneLoader.Instance.LoadSceneWithFadeEffect("QRReader");
            else
                SceneLoader.Instance.LoadSceneWithFadeEffect(nextSceneName);
        }
    }

    IEnumerator Start() {
        yield return new WaitForSeconds(splashScreenDelayTime);

        if (Application.platform == RuntimePlatform.Android) {
            if (Permission.HasUserAuthorizedPermission(Permission.Camera))
                SceneLoader.Instance.LoadSceneWithFadeEffect("QRReader");
            else {
                Permission.RequestUserPermission(Permission.Camera);
                hasRequestedPermission = true;
            }

        } else
            SceneLoader.Instance.LoadSceneWithFadeEffect("QRReader");
    }
}